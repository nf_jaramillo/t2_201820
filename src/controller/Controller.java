package controller;

import api.IDivvyTripsManager;
import model.data_structures.ListaDoblementeEncadenada;
import model.logic.DivvyTripsManager;
import model.vo.VOTrip;

public class Controller {

	/**
	 * Reference to the services manager
	 */
	@SuppressWarnings("unused")
	private static IDivvyTripsManager  manager = new DivvyTripsManager();
	
	
	public static void loadStations() {
		
		manager.loadStations("./data/Divvy_Stations_2017_Q3Q4.csv");
	}
	
	public static void loadTrips() {
		manager.loadTrips("./data/Divvy_Trips_2017_Q4.csv");
		
	}
		
	public static ListaDoblementeEncadenada <VOTrip> getTripsOfGender (String gender) {
		return manager.getTripsOfGender(gender);
	}
	
	public static ListaDoblementeEncadenada <VOTrip> getTripsToStation (int stationID) {
		return manager.getTripsToStation(stationID);
	}
}
