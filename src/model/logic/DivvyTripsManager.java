package model.logic;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import com.opencsv.CSVReader;

import api.IDivvyTripsManager;
import model.vo.VOStation;
import model.vo.VOTrip;
import model.data_structures.ListaDoblementeEncadenada;
import model.data_structures.NodoListaDoble;
import model.data_structures.NodoListaSencilla;
import model.data_structures.UnicamenteIdentificado;

public class DivvyTripsManager implements IDivvyTripsManager {

	
	//---------------------------------------------
	//Atributos
	//--------------------------------------------
	ListaDoblementeEncadenada<VOTrip> trips = new ListaDoblementeEncadenada<VOTrip>();
	ListaDoblementeEncadenada<UnicamenteIdentificado> stations = new ListaDoblementeEncadenada<UnicamenteIdentificado>();
	
	
	public void loadStations (String stationsFile) {
		CSVReader reader = null;
		try {
			reader = new CSVReader(new FileReader(stationsFile));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	     String [] nextLine;
	     try {
	    	 int i = 0;
	    	 
			while ((nextLine = reader.readNext()) != null) {
				VOStation este = new VOStation(nextLine[0]);
				
				stations.add(0,este);
			    // nextLine[] is an array of values from the line
			    System.out.println(nextLine[0] + nextLine[1] + "etc...");
			 }
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	
	public void loadTrips (String tripsFile) {
		CSVReader reader = null;
		try {
			reader = new CSVReader(new FileReader(tripsFile));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	     String[]  nextLine;
	     try {
	    	 int i = 0;
	    	 
			while ((nextLine = reader.readNext()) != null) {
				if(i>0)
				{
					
					String info = nextLine[0]+","+nextLine[1]+","+nextLine[2]+","+nextLine[3]+","+nextLine[4]+","+nextLine[5]+","+nextLine[6]+","+nextLine[7]+","+nextLine[8]+","+nextLine[9]+","+nextLine[10]+","+nextLine[11];
					
					VOTrip este = new VOTrip(info);
					
					trips.add(0,este);
				    // nextLine[] is an array of values from the line
				    System.out.println(nextLine[0] + nextLine[1] + "etc...");
				}
				i++;
				
			 }
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	
	
	
	public ListaDoblementeEncadenada<VOTrip> getTripsOfGender (String gender) {
		VOTrip temp = null;
		ListaDoblementeEncadenada<VOTrip> resp = new ListaDoblementeEncadenada<>();
		NodoListaSencilla<VOTrip> nodo = null;
		int cant = trips.size();
		if(cant>0)
		{
			nodo = trips.darNodo(0);
		}
		for (int i = 0; i < cant; i++) {
			
			temp = nodo.darElemento();
			String genero = temp.gender();
			if(temp!=null&&genero!=null&&genero.equals(gender))
			{
//				System.out.println(temp.gender());
				resp.add(0, temp);
			}
			nodo = nodo.darSiguiente();
		}
		return resp;
	}

	
	public ListaDoblementeEncadenada <VOTrip> getTripsToStation (int stationID) {
		VOTrip temp = null;
		ListaDoblementeEncadenada<VOTrip> resp = new ListaDoblementeEncadenada<>();
		NodoListaSencilla<VOTrip> nodo = null;
		int cant = trips.size();
		if(cant>0)
		{
			nodo = trips.darNodo(0);
		}
		
		for (int i = 0; i < cant; i++) {
			
			temp = nodo.darElemento();
			int station = temp.station2Id();
			if(temp!=null&&station==(stationID))
			{
//				System.out.println(temp.gender());
				resp.add(0, temp);
			}
			nodo = nodo.darSiguiente();
		}
		return resp;
	}	


}
	