package model.vo;

import model.data_structures.UnicamenteIdentificado;

/**
 * Representation of a Trip object
 */
public class VOTrip implements UnicamenteIdentificado {

	//----------------------------------------
	//ATRIBUTOS
	//---------------------------
	private String info;
	private int id;
	private double seconds;
	private String station1;
	private String station2;
	private String gender;
	private int station2Id;
	
	
	public VOTrip (String pInfo)
	{
		String[] cut = pInfo.split(",");
		info = pInfo;
		id = Integer.valueOf(cut[0]);
		seconds = Double.valueOf(cut[4]);
		station1 = cut[6];
		station2 = cut[8];
		station2Id = Integer.valueOf(cut[7]);
		if(cut.length>10)
		{
			gender = pInfo.split(",")[10];
		}
		
	}
	/**
	 * @return id - Trip_id
	 */
	public int id() {
		// TODO Auto-generated method stub
		return id;
	}	
	
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public double getTripSeconds() {
		// TODO Auto-generated method stub
		return seconds;
	}

	/**
	 * @return station_name - Origin Station Name .
	 */
	public String getFromStation() {
		// TODO Auto-generated method stub	
		return station1;
	}
	
	/**
	 * @return station_name - Destination Station Name .
	 */
	public String getToStation() {
		// TODO Auto-generated method stub
		return station2;
	}
	/**
	 * @return station_id - Destination Station id .
	 */
	public int station2Id() {
		// TODO Auto-generated method stub
		return station2Id;
	}
	
	/**
	 * @return gender.
	 */
	public String gender() {
		
		return gender;
	}
	/**
	 * @return info.
	 */
	public String darInfo() {
		
		return info;
	}
	@Override
	public String darIdentificador() {
		// TODO Auto-generated method stub
		return String.valueOf(id);
	}
}
