package model.data_structures;

import java.io.Serializable;
import java.util.ListIterator;

/**
 * Clase que representa el iterador de lista (avanza hacia adelante y hacia atrás)
 * @param <E> Tipo del objeto que almacena el iterador de la lista
 */
public class IteradorLista<E extends UnicamenteIdentificado> implements ListIterator<E>, Serializable 
{

	/**
	 * Constante de serialización
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Nodo anterior al que se encuentra el iterador.
	 */
	private NodoListaDoble<E> anterior;

	/**
	 * Nodo en el que se encuentra el iterador.
	 */
	private NodoListaDoble<E> actual;
	
	/**
     * Crea un nuevo iterador de lista iniciando en el nodo que llega por parámetro
     * @param primerNodo el nodo en el que inicia el iterador. nActual != null
     */
	public IteradorLista(NodoListaDoble<E> primerNodo)
	{
		actual = primerNodo;
		anterior = null;
	}
	
    /**
     * Indica si hay nodo siguiente
     * true en caso que haya nodo siguiente o false en caso contrario
     */
	public boolean hasNext() 
	{
		boolean resp = actual.darSiguiente()!=null? true:false; 
		return resp;
	}

    /**
     * Indica si hay nodo anterior
     * true en caso que haya nodo anterior o false en caso contrario
     */
	public boolean hasPrevious() 
	{
		boolean resp = actual.darAnterior()!=null? true:false; 
		return resp;
	}

    /**
     * Devuelve el elemento siguiente de la iteración y avanza.
     * @return elemento siguiente de la iteración
     */
	public E next() 
	{
		actual = (NodoListaDoble<E>) actual.darSiguiente();
		return actual.darElemento();
	}

    /**
     * Devuelve el elemento anterior de la iteración y retrocede.
     * @return elemento anterior de la iteración.
     */
	public E previous() 
	{
		actual = (NodoListaDoble<E>) actual.darAnterior();
		return actual.darElemento();
	}
	
	
	//=======================================================
	// Métodos que no se implementarán
	//=======================================================
	
	public int nextIndex() 
	{
		throw new UnsupportedOperationException();
	}

	public int previousIndex() 
	{
		throw new UnsupportedOperationException();
	}

	public void remove() 
	{
		throw new UnsupportedOperationException();
	}

	public void set(E e) 
	{
		throw new UnsupportedOperationException();
	}
	
	public void add(E e) 
	{
		throw new UnsupportedOperationException();		
	}

}
