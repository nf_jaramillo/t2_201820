package model.data_structures;

public class NodoListaDoble<E extends UnicamenteIdentificado> extends NodoListaSencilla<E>
{
	/**
	 * Constante de serialización
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Nodo anterior.
	 */
	private NodoListaDoble anterior;
	
	/**
	 * Método constructor del nodo doblemente encadenado
	 * @param elemento elemento que se almacenará en el nodo.
	 */

	public NodoListaDoble(E pElemento) 
	{
		super(pElemento);
		anterior = null;
		
	}

	/**
	 * Método que retorna el nodo anterior.
	 * @return Nodo anterior.
	 */
	public NodoListaDoble<E> darAnterior()
	{
		return anterior;
	}
	
	/**
	 * Método que cambia el nodo anterior por el que llega como parámetro.
	 * @param anterior Nuevo nodo anterior.
	 */
	public void cambiarAnterior(NodoListaDoble<E> pAnterior)
	{
		anterior = pAnterior;
	}
}
